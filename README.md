# Kazarma Helm chart

## Install

```bash
helm repo add technostructures https://technostructures.gitlab.io/helm-charts
helm install my-release technostructures/kazarma
```

## Uninstall

```bash
helm delete my-release
```

The command removes all the Kubernetes components but PVC's associated with the chart and deletes the release.

To delete the PVC's associated with `my-release`:

```bash
kubectl delete pvc -l release=my-release
```

## Parameters

| Name                                                 | Description                                                                                | Value                                            |
|:---------------------------------------------------- | ------------------------------------------------------------------------------------------ | ------------------------------------------------ |
| `kazarma.database.username`                          | Postgres username                                                                          | `kazarma`                                        |
| `kazarma.database.database`                          | Postgres database                                                                          | `kazarma`                                        |
| `kazarma.matrixUrl`                                  | Matrix server URL                                                                          | `https://matrix.example.com`                     |
| `kazarma.apDomain`                                   | ActivityPub domain                                                                         | `example.com`                                    |
| `kazarma.puppetPrefix`                               | Prefix for Matrix puppets username, for example, "ap_"                                     | `_ap_`                                           |
| `kazarma.host`                                       | Kazarma hostname                                                                           | `kazarma.example.com`                            |
| `kazarma.bridgeRemote`                               | Bridge federated Matrix users                                                              | `false`                                          |
| `kazarma.htmlSearch`                                 | Allow searching on the Web frontend                                                        | `true`                                           |
| `kazarma.htmlAp`                                     | Show ActivityPub users profile page on the Web frontend                                    | `true`                                           |
| `kazarma.logLevel`                                   | Log level                                                                                  | `warning`                                        |
| `kazarma.releaseLevel`                               | Release level                                                                              | `prod`                                           |
| `kazarma.sentry.enabled`                             | Enable Sentry integration                                                                  | `false`                                          |
| `kazarma.sentry.dsn`                                 | Sentry DSN                                                                                 | `""`                                             |
| `kazarma.loki.enabled`                               | Enable Loki integration                                                                    | `false`                                          |
| `kazarma.loki.host`                                  | Loki host                                                                                  | `""`                                             |
| `kazarma.loki.orgId`                                 | Loki org ID                                                                                | `""`                                             |
| `kazarma.metrics.enabled`                            | Enable metrics endpoint                                                                    | `false`                                          |
| `kazarma.metrics.port`                               | Metrics endpoint port                                                                      | `4021`                                           |
| `kazarma.metrics.grafana.enabled`                    | Enable automatic send of Grafana dashboards                                                | `false`                                          |
| `kazarma.metrics.grafana.host`                       | Grafana host                                                                               | `""`                                             |
| `kazarma.metrics.grafana.token`                      | Grafana API token                                                                          | `""`                                             |
| `kazarma.appservice`                                 | (keys used to generate the appservice configuration file, see below for more informations) |                                                  |
| `kazarma.appservice.name`                            | Name of the appservice                                                                     | `Kazarma`                                        |
| `kazarma.appservice.url`                             | URL of the appservice                                                                      | `https://kazarma.example.com/matrix/`            |
| `kazarma.appservice.senderLocalpart`                 | Local part of the "bridge bot"                                                             | `_kazarma`                                       |
| `kazarma.appservice.namespaces.aliases`              | (list of alias namespaces)                                                                 |                                                  |
| `kazarma.appservice.namespaces.aliases[0].exclusive` | Prevent regular users from using the namespace                                             | `true`                                           |
| `kazarma.appservice.namespaces.aliases[0].regex`     | Regex for the namespace                                                                    | `#ap_.+___.+:example.com`                        |
| `kazarma.appservice.namespaces.users`                | (list of user namespaces)                                                                  |                                                  |
| `kazarma.appservice.namespaces[0].exclusive`         | Prevent regular users from using the namespace                                             | `true`                                           |
| `kazarma.appservice.namespaces[0].regex`             | Regex for the namespace                                                                    | `@ap_.+___.+:example.com`                        |
| `kazarma.image.repository`                           | Kazarma Docker image                                                                       | `registry.gitlab.com/technostructures/kazarma/kazarma/production` |
| `kazarma.image.pullPolicy`                           | Image pull policy                                                                          | `IfNotPresent`                                   |
| `kazarma.image.tag`                                  | Image tag                                                                                  | `latest`                                         |
| `postgresql.nameOverride`                            | Name override for the postgresql chart                                                     | `kazarma-postgres`                               |
| `postgresql.auth.username`                           | Postgresql username                                                                        | `kazarma`                                        |
| `postgresql.auth.database`                           | Postgresql database                                                                        | `kazarma`                                        |
| `postgresql.auth.existingSecret`                     | Postgresql existing secret                                                                 | `kazarma-postgres-password`                      |


## Appservice configuration file generation

The chart generates a secret named `kazarma-appservices`. The appservice configuration is in its `kazarma.yaml` key.
